package main

import (
	"github.com/rs/zerolog/log"
	"net/http"
)

func resetPassword(w http.ResponseWriter, r *http.Request) {
	query := r.URL.Query()
	token := query["token"]
	if len(token) == 0 {
		log.Error().Msg("No token when calling reset password")
		http.Redirect(w, r, "/error", 302)
		return
	}
	tpl.ExecuteTemplate(w, "reset-password.html", ViewData{ResetToken: token[0]})
}

func processResetPassword(w http.ResponseWriter, r *http.Request) {
	request := map[string]interface{}{}
	request["token"] = r.FormValue("token")
	request["password"] = r.FormValue("password")

	resp, err := makeRequest(request, "/api/v1/auth/forgot-password/complete", nil)
	if err == APIFailureError {
		log.Error().Err(err).Msg(resp.Msg)
		http.Redirect(w, r, "/reset-password?token="+r.FormValue("token"), 302)
		return
	}
	http.Redirect(w, r, "/", 302)
}
