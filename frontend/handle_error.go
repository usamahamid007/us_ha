package main

import (
	"net/http"
)

func handleError(w http.ResponseWriter, r *http.Request) {
	tpl.ExecuteTemplate(w, "error.html", ViewData{})
}

func handleForbidden(w http.ResponseWriter, r *http.Request) {
	tpl.ExecuteTemplate(w, "forbidden.html", ViewData{})
}
