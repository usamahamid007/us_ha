package main

type User struct {
	Email    string
	Provider string
	FullName string `json:"full_name"`
	Address  string
	Phone    string
}

type ViewData struct {
	Authenticated      bool
	Email              string
	Provider           string
	FullName           string
	Address            string
	Phone              string
	IsExternalProvider bool
	ResetToken         string
	GoogleAuthURL      string
}

type LoginResponse struct {
	JWTToken string `json:"jwtToken"`
	User     User   `json:"user"`
	Msg      string `json:"msg"`
}
