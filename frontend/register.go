package main

import (
	"github.com/rs/zerolog/log"
	"net/http"
)

func register(w http.ResponseWriter, r *http.Request) {
	tpl.ExecuteTemplate(w, "register.html", ViewData{
		GoogleAuthURL: google_auth_url,
	})
}

func processRegister(w http.ResponseWriter, r *http.Request) {
	request := map[string]interface{}{}
	request["email"] = r.FormValue("email")
	request["password"] = r.FormValue("password")
	request["full_name"] = r.FormValue("email")

	response, err := makeRequest(request, "/api/v1/auth/register", nil)
	if err != nil {
		log.Error().Err(err).Msg("Failed to make API request")
		http.Redirect(w, r, "/error", 302)
		return
	}

	token := response.JWTToken
	session, _ := store.Get(r, "cookie-name")
	session.Options.MaxAge = 43200
	session.Values["token"] = token
	session.Values["authenticated"] = true
	session.Values["user"] = response.User
	err = session.Save(r, w)

	if err != nil {
		log.Error().Err(err).Msg("Failed to delete session")
		http.Redirect(w, r, "/error", 302)
		return
	}

	http.Redirect(w, r, "/home", 302)
}
