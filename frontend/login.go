package main

import (
	"github.com/rs/zerolog/log"
	"net/http"
)

func login(w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, "cookie-name")
	session.Options.MaxAge = -1
	err := session.Save(r, w)
	if err != nil {
		log.Error().Err(err).Msg("Failed to delete session")
		http.Redirect(w, r, "/error", 302)
	}
	tpl.ExecuteTemplate(w, "login.html", ViewData{
		GoogleAuthURL: google_auth_url,
	})
}

func processLogin(w http.ResponseWriter, r *http.Request) {
	request := map[string]interface{}{}
	request["email"] = r.FormValue("email")
	request["password"] = r.FormValue("password")

	response, err := makeRequest(request, "/api/v1/auth/login", nil)
	if err != nil {
		log.Error().Err(err).Msg("Failed to make API request")
		http.Redirect(w, r, "/error", 302)
		return
	}

	token := response.JWTToken
	session, _ := store.Get(r, "cookie-name")
	session.Options.MaxAge = 43200
	session.Values["token"] = token
	session.Values["authenticated"] = true
	session.Values["user"] = response.User
	err = session.Save(r, w)

	if err != nil {
		log.Error().Err(err).Msg("Failed to save session")
		http.Redirect(w, r, "/error", 302)
		return
	}

	http.Redirect(w, r, "/home", 302)
}

func auth(w http.ResponseWriter, r *http.Request) {
	query := r.URL.Query()
	token := query["token"]

	request := map[string]interface{}{}
	response, err := makeGetRequest(request, "/api/v1/users/me", &token[0])
	if err != nil {
		log.Error().Err(err).Msg("Failed to make API request")
		http.Redirect(w, r, "/error", 302)
		return
	}
	session, _ := store.Get(r, "cookie-name")
	session.Options.MaxAge = 43200
	session.Values["token"] = token[0]
	session.Values["authenticated"] = true
	session.Values["user"] = response.User
	err = session.Save(r, w)
	if err != nil {
		log.Error().Err(err).Msg("Failed to save session")
		http.Redirect(w, r, "/error", 302)
		return
	}

	http.Redirect(w, r, "/home", 302)
}
