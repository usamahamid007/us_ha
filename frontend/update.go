package main

import (
	"github.com/rs/zerolog/log"
	"net/http"
)

func update(w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, "cookie-name")

	if auth, ok := session.Values["authenticated"].(bool); !ok || !auth {
		http.Redirect(w, r, "/forbidden", 302)
		return
	}

	tpl.ExecuteTemplate(w, "update.html", getViewData(session))
}

func processUpdate(w http.ResponseWriter, r *http.Request) {
	request := map[string]interface{}{}
	request["full_name"] = r.FormValue("name")
	request["email"] = r.FormValue("email")
	request["address"] = r.FormValue("address")
	request["phone"] = r.FormValue("phone")


	session, _ := store.Get(r, "cookie-name")
	token := session.Values["token"].(string)
	if request["email"] == "" {
		request["email"] = session.Values["user"].(User).Email
	}
	
	response, err := makeRequest(request, "/api/v1/users/me", &token)
	if err != nil {
		log.Error().Err(err).Msg("Failed to make API request")
		http.Redirect(w, r, "/error", 302)
		return
	}

	session.Values["user"] = response.User
	err = session.Save(r, w)
	if err != nil {
		log.Error().Err(err).Msg("Failed to delete session")
		http.Redirect(w, r, "/error", 302)
		return
	}
	http.Redirect(w, r, "/home", 302)
}
