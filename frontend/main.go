package main

import (
	"context"
	"encoding/gob"
	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	"github.com/joho/godotenv"
	"github.com/rs/zerolog/log"
	"html/template"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func init() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal().Err(err).Msg("Failed to Load ENV")
	}
}

var (
	store           *sessions.CookieStore
	tpl             *template.Template
	router          *mux.Router
	base_url        string
	google_auth_url string
)

func main() {
	gob.Register(User{})
	addr := ":" + os.Getenv("PORT")
	base_url = os.Getenv("API_BASE_URL")
	key := []byte(os.Getenv("SESSION_KEY"))
	google_auth_url = getGoogleAuthURL()
	store = sessions.NewCookieStore(key)

	router = mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/", login)
	router.HandleFunc("/api/login", processLogin)
	router.HandleFunc("/error", handleError)
	router.HandleFunc("/forbidden", handleForbidden)
	router.HandleFunc("/home", home)
	router.HandleFunc("/update", update)
	router.HandleFunc("/api/update", processUpdate)
	router.HandleFunc("/register", register)
	router.HandleFunc("/api/register", processRegister)
	router.HandleFunc("/forgot-password", forgotPassword)
	router.HandleFunc("/api/forgot-password", processForgotPassword)
	router.HandleFunc("/reset-password", resetPassword)
	router.HandleFunc("/api/reset-password", processResetPassword)
	router.HandleFunc("/auth", auth)

	publicDir := "/public/"
	router.PathPrefix(publicDir).Handler(http.StripPrefix(publicDir, http.FileServer(http.Dir(publicDir))))

	tpl = template.New("roottemplate")
	tpl = tpl.Funcs(template.FuncMap{
	})
	tpl = template.Must(tpl.ParseGlob("templates/html/*.html"))

	httpServer := &http.Server{Addr: addr, Handler: router}

	done := make(chan os.Signal, 1)
	signal.Notify(done, os.Interrupt, syscall.SIGTERM)
	go func() {
		if err := httpServer.ListenAndServe(); err != nil {
			if err == http.ErrServerClosed {
				log.Info().Msg("Server close triggered!")
			} else {
				log.Fatal().Err(err).Msg("Failed to start server")
			}
		}
	}()
	<-done

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer func() {
		cancel()
	}()

	if err := httpServer.Shutdown(ctx); err != nil && err != http.ErrServerClosed {
		log.Fatal().Err(err).Msg("Failed to gracefully shut down server")
	}
	log.Info().Msg("Server exited gracefully")
}

func getGoogleAuthURL() string {
	authURL, _ := url.Parse("https://accounts.google.com/o/oauth2/v2/auth")
	query := authURL.Query()
	query.Set("client_id", os.Getenv("GGL_AUTH_CLIENT_ID"))
	query.Set("redirect_uri", os.Getenv("GGL_AUTH_REDIRECT_URL"))
	query.Set("response_type", "code")
	query.Set("scope", "email profile openid")
	authURL.RawQuery = query.Encode()
	return authURL.String()
}
