package main

import "github.com/gorilla/sessions"

func getViewData(session *sessions.Session) ViewData {
	data := ViewData{}
	data.Authenticated = session.Values["authenticated"].(bool)
	if data.Authenticated {
		user := session.Values["user"].(User)
		data.Email = user.Email
		data.Provider = user.Provider
		data.FullName = user.FullName
		data.Address = user.Address
		data.Phone = user.Phone
		data.IsExternalProvider = user.Provider != "Local"
	}
	return data
}
