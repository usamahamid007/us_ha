module bitbucket.org/usamahamid007/us_ha_frontend

go 1.15

require (
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/sessions v1.2.1
	github.com/joho/godotenv v1.3.0
	github.com/rs/zerolog v1.20.0
)
