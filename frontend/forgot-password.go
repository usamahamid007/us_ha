package main

import (
	"github.com/rs/zerolog/log"
	"net/http"
)

func forgotPassword(w http.ResponseWriter, r *http.Request) {
	tpl.ExecuteTemplate(w, "forgot-password.html", ViewData{})
}

func processForgotPassword(w http.ResponseWriter, r *http.Request) {
	request := map[string]interface{}{}
	request["email"] = r.FormValue("email")

	_, err := makeRequest(request, "/api/v1/auth/forgot-password", nil)
	if err == APIFailureError {
		http.Redirect(w, r, "/", 302)
		return
	}

	if err != nil {
		log.Fatal().Err(err).Msg("Failed to make API request")
		http.Redirect(w, r, "/error", 302)
		return
	}

	http.Redirect(w, r, "/", 302)
}

