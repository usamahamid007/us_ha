package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"github.com/rs/zerolog/log"
	"net/http"
)

var (
	APIFailureError = errors.New("API Request failed")
)

func makeRequest(request map[string]interface{}, endpoint string, jwtToken *string) (*LoginResponse, error) {
	respModel := LoginResponse{}
	js, err := json.Marshal(request)
	if err != nil {
		log.Error().Err(err).Msg("Error during parsing error msg response")
		return nil, err
	}
	req, err := http.NewRequest("POST", base_url+endpoint, bytes.NewBuffer(js))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json; charset=utf-8")
	if jwtToken != nil {
		req.Header.Set("Authorization", "Bearer "+*jwtToken)
	}
	resp, err := http.DefaultClient.Do(req)
	err = json.NewDecoder(resp.Body).Decode(&respModel)
	if err != nil {
		log.Error().Err(err).Msg("Error during parsing response payload")
		return nil, err
	}
	if resp.StatusCode >= 200 && resp.StatusCode < 300 {
		return &respModel, nil
	}
	return &respModel, APIFailureError
}

func makeGetRequest(request map[string]interface{}, endpoint string, jwtToken *string) (*LoginResponse, error) {
	respModel := LoginResponse{}
	js, err := json.Marshal(request)
	if err != nil {
		log.Error().Err(err).Msg("Error during parsing error msg response")
		return nil, err
	}
	req, err := http.NewRequest("GET", base_url+endpoint, bytes.NewBuffer(js))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json; charset=utf-8")
	if jwtToken != nil {
		req.Header.Set("Authorization", "Bearer "+*jwtToken)
	}
	resp, err := http.DefaultClient.Do(req)
	err = json.NewDecoder(resp.Body).Decode(&respModel)
	if err != nil {
		log.Error().Err(err).Msg("Error during parsing response payload")
		return nil, err
	}
	if resp.StatusCode >= 200 && resp.StatusCode < 300 {
		return &respModel, nil
	}
	return &respModel, APIFailureError
}
