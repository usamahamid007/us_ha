package main

import (
	"net/http"
)

func home(w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, "cookie-name")

	if auth, ok := session.Values["authenticated"].(bool); !ok || !auth {
		http.Redirect(w, r, "/forbidden", 302)
		return
	}

	tpl.ExecuteTemplate(w, "home.html", getViewData(session))
}
