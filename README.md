## US HA - Test

### Deployment steps
1. Copy the three `.env.template` to `.env` and update them as needed
2. Install Docker and Docker Compose, if you don't have it already 
3. Run `docker-compose up -d`

### Highlights
* Docker Support
* Separate Frontend and Backend (for individualized scaling)

### Dependencies

##### For Backend 
* Router - [github.com/gorilla/mux](https://github.com/gorilla/mux)
* ORM - [gorm.io/gorm](https://gorm.io)
* MySQL Driver - [gorm.io/driver/mysql](https://gorm.io)
* Logger - [github.com/rs/zerolog](https://github.com/rs/zerolog)
* `.env` file loader - [github.com/joho/godotenv](https://github.com/joho/godotenv)
* Testing Library - [github.com/stretchr/testify](https://github.com/stretchr/testify)
* Crypto library- [golang.org/x/crypto](https://godoc.org/golang.org/x/crypto)
* OAuth2 client - [golang.org/x/oauth2](https://github.com/golang/oauth2)
* Email library- [github.com/jordan-wright/email](https://github.com/jordan-wright/email)
* JWT token support - [github.com/dgrijalva/jwt-go](https://github.com/dgrijalva/jwt-go)

##### For Frontend 
* Router - [github.com/gorilla/mux](https://github.com/gorilla/mux)
* Sessions - [github.com/gorilla/sessions](https://github.com/gorilla/sessions)
* Logger - [github.com/rs/zerolog](https://github.com/rs/zerolog)
* `.env` file loader - [github.com/joho/godotenv](https://github.com/joho/godotenv)
* CSS framework - [BootStrap](https://getbootstrap.com/)

##### TODO
* Refactor/Cleanup Frontend for better code stucture 
* Implement Error Msging in frontend on API errors, instead of just redirecting to Error page
*  