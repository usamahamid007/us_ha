module bitbucket.org/usamahamid007/us_ha

go 1.15

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gorilla/mux v1.8.0
	github.com/joho/godotenv v1.3.0
	github.com/jordan-wright/email v4.0.1-0.20200917010138-e1c00e156980+incompatible
	github.com/rs/zerolog v1.20.0
	github.com/stretchr/testify v1.6.1
	golang.org/x/crypto v0.0.0-20201016220609-9e8e0b390897
	golang.org/x/oauth2 v0.0.0-20200902213428-5d25da1a8d43
	gorm.io/driver/mysql v1.0.3
	gorm.io/gorm v1.20.5
)
