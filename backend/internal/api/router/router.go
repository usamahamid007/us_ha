package router

import (
	"bitbucket.org/usamahamid007/us_ha/internal/api"
	"bitbucket.org/usamahamid007/us_ha/internal/api/handlers"
	"bitbucket.org/usamahamid007/us_ha/internal/api/middleware"
	"github.com/gorilla/mux"
)

func Init(s *api.Server) {
	s.MuxRouter = mux.NewRouter()

	apiV1Auth := s.MuxRouter.PathPrefix("/api/v1/auth").Subrouter()

	apiV1API := s.MuxRouter.PathPrefix("/api/v1").Subrouter()
	apiV1API.Use(middleware.JWTMiddleware(s))

	s.SubRouters = &api.SubRouters{
		Root:      s.MuxRouter,
		APIV1Auth: apiV1Auth,
		ApiV1API:  apiV1API,
	}
	handlers.AttachAllRoutes(s)
}
