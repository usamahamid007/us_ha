package api

import (
	"bitbucket.org/usamahamid007/us_ha/internal/config"
	"bitbucket.org/usamahamid007/us_ha/internal/mailer"
	"bitbucket.org/usamahamid007/us_ha/internal/models"
	"context"
	"github.com/gorilla/mux"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"html/template"
	"net"
)

type Server struct {
	Config      config.ServerConfig
	MuxRouter   *mux.Router
	SubRouters  *SubRouters
	OAuthConfig *oauth2.Config
	Database    *gorm.DB
	Mailer      *mailer.Mailer
}

type SubRouters struct {
	Root      *mux.Router
	APIV1Auth *mux.Router
	ApiV1API  *mux.Router
}

func NewServer(config config.ServerConfig) *Server {
	s := &Server{
		Config:      config,
		MuxRouter:   nil,
		SubRouters:  nil,
		OAuthConfig: nil,
		Database:    nil,
		Mailer:      nil,
	}
	return s
}

func (s *Server) InitOAuthConfig() {
	s.OAuthConfig = &oauth2.Config{
		ClientID:     s.Config.GglAuthConfig.ClientID,
		ClientSecret: s.Config.GglAuthConfig.ClientSecret,
		RedirectURL:  s.Config.GglAuthConfig.RedirectURL,
		Scopes: []string{
			`email`,
			`profile`,
			`openid`,
		},
		Endpoint: google.Endpoint,
	}
}

func (s *Server) InitDB(ctx context.Context) error {
	dsn := s.Config.DBConfig.DSN
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		return err
	}
	s.Database = db
	if s.Config.DBConfig.AutoMigrate {
		err = s.Database.AutoMigrate(&models.User{}, &models.PasswordResetToken{})
	}
	return err
}

func (s *Server) HandleShutdown() {
	// No action for now
}

func (s *Server) InitMailer() error {
	s.Mailer = &mailer.Mailer{
		Config:    s.Config.Mailer,
		Templates: map[string]*template.Template{},
		Addr:      net.JoinHostPort(s.Config.Mailer.Host, s.Config.Mailer.Port),
	}
	return s.Mailer.ParseTemplates()
}
