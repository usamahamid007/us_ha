package handlers

import (
	"bitbucket.org/usamahamid007/us_ha/internal/api"
	"bitbucket.org/usamahamid007/us_ha/internal/api/handlers/auth"
	"bitbucket.org/usamahamid007/us_ha/internal/api/handlers/common"
)

func AttachAllRoutes(s *api.Server) {
	// attach our routes
	auth.PostLoginRoute(s)
	auth.PostRegisterRoute(s)
	auth.PostForgotPasswordRoute(s)
	auth.PostForgotPasswordCompleteRoute(s)
	auth.PostGoogleAuthRoute(s)
	common.PutUpdateUserRoute(s)
	common.GetCurrentUserRoute(s)
}
