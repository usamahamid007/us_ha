package auth

import (
	"bitbucket.org/usamahamid007/us_ha/internal/api"
	"bitbucket.org/usamahamid007/us_ha/internal/models"
	"bitbucket.org/usamahamid007/us_ha/internal/models/types"
	"bitbucket.org/usamahamid007/us_ha/internal/util"
	"github.com/rs/zerolog/log"
	"net/http"
)

// TODO: Move to ENV

func PostForgotPasswordCompleteRoute(s *api.Server) {
	s.SubRouters.APIV1Auth.HandleFunc("/forgot-password/complete", postForgotPasswordCompleteHandler(s)).Methods("POST")
}

func postForgotPasswordCompleteHandler(s *api.Server) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		request := types.PostForgotPasswordCompleteRequest{}
		util.ParseJSONRequest(r, &request)
		if len(request.Password) == 0 {
			util.HandleErrorResponse(w, http.StatusBadRequest, "Missing password")
			return
		}

		if len(request.Password) < MinPasswordLength {
			util.HandleErrorResponse(w, http.StatusBadRequest, "Password should be at least 8 characters")
			return
		}

		resetToken := models.PasswordResetToken{}
		result := s.Database.First(&resetToken, `token = ?`, request.Token)

		if result.Error != nil || result.RowsAffected == 0 {
			util.HandleErrorResponse(w, http.StatusBadRequest, "Invalid reset token")
			return
		}
		_, err := util.ValidateToken(request.Token, s.Config.JWTSecret, s.Config.ResetPasswordDuration)
		if err != nil {
			util.HandleErrorResponse(w, http.StatusUnauthorized, err.Error())
			return
		}

		user := models.User{}
		result = s.Database.First(&user, `id = ?`, resetToken.UserID)
		if result.Error != nil || result.RowsAffected == 0 {
			log.Error().Err(result.Error).Msg("Missing User for token!")
			util.HandleErrorResponse(w, http.StatusBadRequest, "Missing User for token")
			return
		}

		passwordHash, err := util.HashPassword(request.Password)
		if err != nil {
			log.Error().Err(err).Msg("Error hashing password!")
			util.HandleErrorResponse(w, http.StatusInternalServerError, "Error hashing password!")
			return
		}

		user.Password = passwordHash
		result = s.Database.Save(&user)
		if result.Error != nil || result.RowsAffected == 0 {
			log.Error().Err(result.Error).Msg("Error updating User!")
			util.HandleErrorResponse(w, http.StatusInternalServerError, "Error updating User!")
			return
		}

		result = s.Database.Delete(&resetToken)
		if result.Error != nil || result.RowsAffected == 0 {
			log.Error().Err(result.Error).Msg("Error deleting used reset token!")
			util.HandleErrorResponse(w, http.StatusInternalServerError, "Error deleting used reset token!")
			return
		}

		response := types.CommonMsgResponse{Msg: "Successfully send email!"}
		util.MarshalJSONResponse(w, response)
	}
}
