package auth

import (
	"bitbucket.org/usamahamid007/us_ha/internal/api"
	"bitbucket.org/usamahamid007/us_ha/internal/models"
	"bitbucket.org/usamahamid007/us_ha/internal/models/types"
	"bitbucket.org/usamahamid007/us_ha/internal/util"
	"encoding/json"
	"github.com/rs/zerolog/log"
	"golang.org/x/oauth2"
	"net/http"
	"net/url"
)

func PostGoogleAuthRoute(s *api.Server) {
	s.SubRouters.APIV1Auth.HandleFunc("/google/callback", postGoogleHandler(s)).Methods("GET")
}

func postGoogleHandler(s *api.Server) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		params := r.URL.Query()
		code := params["code"]
		if len(code) == 0 {
			util.HandleErrorResponse(w, http.StatusBadRequest, "No Auth Code!")
			return
		}
		gglToken, err := s.OAuthConfig.Exchange(oauth2.NoContext, code[0])
		if err != nil {
			util.HandleErrorResponse(w, http.StatusBadRequest, "Invalid Auth Code!")
			return
		}
		client := s.OAuthConfig.Client(oauth2.NoContext, gglToken)
		gglResp, err := client.Get("https://www.googleapis.com/oauth2/v3/userinfo")
		if err != nil {
			util.HandleErrorResponse(w, http.StatusBadRequest, "Invalid Auth Code!")
			return
		}
		defer gglResp.Body.Close()

		request := types.GoogleProfileResponse{}
		err = json.NewDecoder(gglResp.Body).Decode(&request)
		if err != nil {
			util.HandleErrorResponse(w, http.StatusBadRequest, "Invalid Auth Code!")
			return
		}

		user := models.User{}
		result := s.Database.First(&user, `email = ?`, request.Email)
		if result.RowsAffected == 0 {
			user = models.User{
				Email:    request.Email,
				FullName: request.FullName,
				Provider: "Google"}
			result = s.Database.Create(&user)
			if result.Error != nil || result.RowsAffected == 0 {
				log.Error().Err(err).Msg("Error creating User!")
				util.HandleErrorResponse(w, http.StatusInternalServerError, "Error creating User!")
				return
			}
		}

		token, err := util.GenerateToken(user.ID, request.Email, s.Config.JWTSecret, s.Config.JWTExpiry)
		if err != nil {
			log.Error().Err(err).Msg("Error on generating jwt token!")
			util.HandleErrorResponse(w, http.StatusInternalServerError, "Error on generating jwt token!")
			return
		}

		authUrl, err := url.Parse(s.Config.AuthRedirectURL)
		if err != nil {
			log.Error().Err(err).Msg("Failed to parse password reset URL")
			util.HandleErrorResponse(w, http.StatusInternalServerError, "Failed to send reset password mail")
			return
		}

		query := authUrl.Query()
		query.Set("token", token)
		authUrl.RawQuery = query.Encode()

		http.Redirect(w, r, authUrl.String(), 302)
	}
}
