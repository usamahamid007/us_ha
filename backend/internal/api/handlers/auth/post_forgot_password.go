package auth

import (
	"bitbucket.org/usamahamid007/us_ha/internal/api"
	"bitbucket.org/usamahamid007/us_ha/internal/models"
	"bitbucket.org/usamahamid007/us_ha/internal/models/types"
	"bitbucket.org/usamahamid007/us_ha/internal/util"
	"github.com/rs/zerolog/log"
	"net/http"
	"net/url"
)

// TODO: Move to ENV
const RandTokenLength uint32 = 8

func PostForgotPasswordRoute(s *api.Server) {
	s.SubRouters.APIV1Auth.HandleFunc("/forgot-password", postForgotPasswordHandler(s)).Methods("POST")
}

func postForgotPasswordHandler(s *api.Server) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		request := types.PostForgotPasswordRequest{}
		util.ParseJSONRequest(r, &request)
		if len(request.Email) == 0 {
			util.HandleErrorResponse(w, http.StatusBadRequest, "Missing email")
			return
		}

		if !util.IsEmailValid(request.Email) {
			util.HandleErrorResponse(w, http.StatusBadRequest, "Invalid Email")
			return
		}

		user := models.User{}
		result := s.Database.First(&user, `email = ?`, request.Email)

		if result.Error != nil || result.RowsAffected == 0 {
			util.HandleErrorResponse(w, http.StatusBadRequest, "Invalid User")
			return
		}

		passwordResetToken := models.PasswordResetToken{}
		result = s.Database.First(&passwordResetToken, `user_id = ?`, user.ID)

		token, err := util.GenerateToken(user.ID, user.Email, s.Config.JWTSecret, s.Config.ResetPasswordDuration)
		if err != nil {
			log.Error().Err(err).Msg("Failed to save User's reset token")
			util.HandleErrorResponse(w, http.StatusInternalServerError, "Failed to send reset password mail")
			return
		}

		passwordResetToken.Token = token
		passwordResetToken.UserID = user.ID

		if result.RowsAffected > 0 {
			result = s.Database.Save(&passwordResetToken)
		} else {
			result = s.Database.Create(&passwordResetToken)
		}
		if result.Error != nil {
			log.Error().Err(result.Error).Msg("Failed to save User's reset token")
			util.HandleErrorResponse(w, http.StatusInternalServerError, "Failed to send reset password mail")
			return
		}

		resetUrl, err := url.Parse(s.Config.ResetPasswordURL)
		if err != nil {
			log.Error().Err(err).Msg("Failed to parse password reset URL")
			util.HandleErrorResponse(w, http.StatusInternalServerError, "Failed to send reset password mail")
			return
		}

		query := resetUrl.Query()
		query.Set("token", passwordResetToken.Token)
		resetUrl.RawQuery = query.Encode()

		if err := s.Mailer.SendPasswordReset(user, resetUrl.String()); err != nil {
			log.Error().Err(err).Msg("Failed to send password reset email")
			util.HandleErrorResponse(w, http.StatusInternalServerError, "Failed to send reset password mail")
			return
		}

		response := types.CommonMsgResponse{Msg: "Successfully send email!"}
		util.MarshalJSONResponse(w, response)
	}
}
