package auth

import (
	"bitbucket.org/usamahamid007/us_ha/internal/api"
	"bitbucket.org/usamahamid007/us_ha/internal/models"
	"bitbucket.org/usamahamid007/us_ha/internal/models/types"
	"bitbucket.org/usamahamid007/us_ha/internal/util"
	"github.com/rs/zerolog/log"
	"net/http"
)

const MinPasswordLength = 8

func PostRegisterRoute(s *api.Server) {
	s.SubRouters.APIV1Auth.HandleFunc("/register", postRegisterHandler(s)).Methods("POST")
}

func postRegisterHandler(s *api.Server) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		request := types.PostRegisterRequest{}
		util.ParseJSONRequest(r, &request)
		if len(request.FullName) == 0 || len(request.Email) == 0 {
			util.HandleErrorResponse(w, http.StatusBadRequest, "Missing name, email or password")
			return
		}

		if !util.IsEmailValid(request.Email) {
			util.HandleErrorResponse(w, http.StatusBadRequest, "Invalid Email")
			return
		}

		if len(request.Password) < MinPasswordLength {
			util.HandleErrorResponse(w, http.StatusBadRequest, "Password should be at least 8 characters")
			return
		}

		passwordHash, err := util.HashPassword(request.Password)
		if err != nil {
			log.Error().Err(err).Msg("Error hashing password!")
			util.HandleErrorResponse(w, http.StatusInternalServerError, "Error hashing password!")
			return
		}

		user := models.User{}
		result := s.Database.First(&user, `email = ?`, request.Email)
		if result.RowsAffected > 0 {
			util.HandleErrorResponse(w, http.StatusBadRequest, "Email already registered!")
			return
		}

		user = models.User{
			Email:    request.Email,
			Password: passwordHash,
			FullName: request.FullName,
			Address:  request.Address,
			Phone:    request.Phone,
			Provider: "Local"}

		result = s.Database.Create(&user)

		if result.Error != nil || result.RowsAffected == 0 {
			log.Error().Err(err).Msg("Error creating User!")
			util.HandleErrorResponse(w, http.StatusInternalServerError, "Error creating User!")
			return
		}

		response := types.PostRegisterResponse{}
		token, err := util.GenerateToken(user.ID, request.Email, s.Config.JWTSecret, s.Config.JWTExpiry)
		if err != nil {
			log.Error().Err(err).Msg("Error on generating jwt token!")
			util.HandleErrorResponse(w, http.StatusInternalServerError, "Error on generating jwt token!")
			return
		}

		response.JWTToken = token
		response.User = user
		util.MarshalJSONResponse(w, response)
	}
}
