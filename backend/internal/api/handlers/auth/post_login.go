package auth

import (
	"bitbucket.org/usamahamid007/us_ha/internal/api"
	"bitbucket.org/usamahamid007/us_ha/internal/models"
	"bitbucket.org/usamahamid007/us_ha/internal/models/types"
	"bitbucket.org/usamahamid007/us_ha/internal/util"
	"github.com/rs/zerolog/log"
	"net/http"
)

func PostLoginRoute(s *api.Server) {
	s.SubRouters.APIV1Auth.HandleFunc("/login", postLoginHandler(s)).Methods("POST")
}

func postLoginHandler(s *api.Server) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		request := types.PostLoginRequest{}
		util.ParseJSONRequest(r, &request)
		if len(request.Email) == 0 || len(request.Password) == 0 {
			util.HandleErrorResponse(w, http.StatusBadRequest, "Missing email or password")
			return
		}

		if !util.IsEmailValid(request.Email) {
			util.HandleErrorResponse(w, http.StatusBadRequest, "Invalid Email")
			return
		}

		user := models.User{}
		result := s.Database.First(&user, `email = ?`, request.Email)

		if result.Error != nil || result.RowsAffected == 0 {
			util.HandleErrorResponse(w, http.StatusBadRequest, "Invalid User")
			return
		}

		err := util.ComparePasswordAndHash(request.Password, user.Password)
		if err != nil {
			util.HandleErrorResponse(w, http.StatusBadRequest, "Incorrect Password")
			return
		}

		response := types.PostLoginResponse{}
		token, err := util.GenerateToken(user.ID, request.Email, s.Config.JWTSecret, s.Config.JWTExpiry)
		if err != nil {
			log.Error().Err(err).Msg("Error on generating jwt token!")
			util.HandleErrorResponse(w, http.StatusInternalServerError, "Error on generating jwt token!")
			return
		}

		response.JWTToken = token
		response.User = user
		util.MarshalJSONResponse(w, response)
	}
}
