package common

import (
	"bitbucket.org/usamahamid007/us_ha/internal/api"
	"bitbucket.org/usamahamid007/us_ha/internal/models"
	"bitbucket.org/usamahamid007/us_ha/internal/models/types"
	"bitbucket.org/usamahamid007/us_ha/internal/util"
	"github.com/rs/zerolog/log"
	"net/http"
)

func PutUpdateUserRoute(s *api.Server) {
	s.SubRouters.ApiV1API.HandleFunc("/users/me", putUpdateUserHandler(s)).Methods("POST")
}

func putUpdateUserHandler(s *api.Server) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		request := types.PostUpdateUserRequest{}
		util.ParseJSONRequest(r, &request)
		if len(request.FullName) == 0 || len(request.Email) == 0 || len(request.Email) == 0 {
			util.HandleErrorResponse(w, http.StatusBadRequest, "Missing name, email or password")
			return
		}

		if !util.IsEmailValid(request.Email) {
			util.HandleErrorResponse(w, http.StatusBadRequest, "Invalid Email")
			return
		}

		user := models.User{}
		result := s.Database.First(&user, `id = ?`, r.Header.Get(util.UserIdKey))
		if result.RowsAffected == 0 {
			util.HandleErrorResponse(w, http.StatusBadRequest, "User not registered!")
			return
		}

		user.FullName = request.FullName
		user.Email = request.Email
		user.Address = request.Address
		user.Phone = request.Phone

		result = s.Database.Save(&user)

		if result.Error != nil || result.RowsAffected == 0 {
			log.Error().Err(result.Error).Msg("Error updating User!")
			util.HandleErrorResponse(w, http.StatusInternalServerError, "Error updating User!")
			return
		}

		response := types.PostUpdateUserResponse{}
		response.User = user
		util.MarshalJSONResponse(w, response)
	}
}
