package common

import (
	"bitbucket.org/usamahamid007/us_ha/internal/api"
	"bitbucket.org/usamahamid007/us_ha/internal/models"
	"bitbucket.org/usamahamid007/us_ha/internal/models/types"
	"bitbucket.org/usamahamid007/us_ha/internal/util"
	"net/http"
)

func GetCurrentUserRoute(s *api.Server) {
	s.SubRouters.ApiV1API.HandleFunc("/users/me", getCurrentUserHandler(s)).Methods("GET")
}

func getCurrentUserHandler(s *api.Server) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		user := models.User{}
		result := s.Database.First(&user, `id = ?`, r.Header.Get(util.UserIdKey))
		if result.RowsAffected == 0 {
			util.HandleErrorResponse(w, http.StatusBadRequest, "User not registered!")
			return
		}

		response := types.GetCurrentUserResponse{}
		response.User = user
		util.MarshalJSONResponse(w, response)
	}
}
