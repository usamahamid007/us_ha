package middleware

import (
	"bitbucket.org/usamahamid007/us_ha/internal/api"
	"bitbucket.org/usamahamid007/us_ha/internal/util"
	"github.com/dgrijalva/jwt-go"
	"net/http"
	"strings"
)

func JWTMiddleware(s *api.Server) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

			tokenString := r.Header.Get("Authorization")
			if len(tokenString) == 0 {
				util.HandleErrorResponse(w, http.StatusUnauthorized, "Missing Auth Token")
				return
			}
			tokenString = strings.Replace(tokenString, "Bearer ", "", 1)
			claims, err := util.ValidateToken(tokenString, s.Config.JWTSecret, s.Config.JWTExpiry)
			if err != nil {
				util.HandleErrorResponse(w, http.StatusUnauthorized, err.Error())
				return
			}
			userId := claims.(jwt.MapClaims)[util.UserIdKey].(string)
			email := claims.(jwt.MapClaims)[util.EmailKey].(string)

			r.Header.Set(util.UserIdKey, userId)
			r.Header.Set(util.EmailKey, email)

			next.ServeHTTP(w, r)
		})
	}
}
