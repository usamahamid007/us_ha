package mailer

import (
	"bitbucket.org/usamahamid007/us_ha/internal/config"
	"bitbucket.org/usamahamid007/us_ha/internal/models"
	"bytes"
	"crypto/tls"
	"errors"
	"github.com/jordan-wright/email"
	"github.com/rs/zerolog/log"
	"html/template"
	"io/ioutil"
	"net/smtp"
	"path/filepath"
)

var (
	ErrEmailTemplateNotFound   = errors.New("email template not found")
	emailTemplatePasswordReset = "password_reset" // /app/templates/email/password_reset/**.
)

type Mailer struct {
	Config    config.MailerConfig
	Templates map[string]*template.Template
	Addr      string
	TLSConfig *tls.Config `json:"-"` // pointer
}

func (m *Mailer) ParseTemplates() error {
	files, err := ioutil.ReadDir(m.Config.BaseTemplateDir)
	if err != nil {
		log.Error().Str("dir", m.Config.BaseTemplateDir).Err(err).Msg("Failed to read email templates directory while parsing templates")
		return err
	}

	for _, file := range files {
		if !file.IsDir() {
			continue
		}

		t, err := template.ParseGlob(filepath.Join(m.Config.BaseTemplateDir, file.Name(), "**"))
		if err != nil {
			log.Error().Str("template", file.Name()).Err(err).Msg("Failed to parse email template files as glob")
			return err
		}

		m.Templates[file.Name()] = t
	}
	return nil
}

func (m *Mailer) SendPasswordReset(user models.User, passwordResetLink string) error {

	t, ok := m.Templates[emailTemplatePasswordReset]
	if !ok {
		log.Error().Msg("Password reset email template not found")
		return ErrEmailTemplateNotFound
	}

	data := map[string]interface{}{
		"full_name":         user.FullName,
		"email":             user.Email,
		"passwordResetLink": passwordResetLink,
	}

	var buf bytes.Buffer
	if err := t.Execute(&buf, data); err != nil {
		log.Error().Err(err).Msg("Failed to execute password reset email template")
		return err
	}

	e := &email.Email{
		To:      []string{user.Email},
		From:    m.Config.DefaultSender,
		Subject: "Password reset",
		HTML:    buf.Bytes(),
	}
	smtpAuth := smtp.PlainAuth("", m.Config.Username, m.Config.Password, m.Config.Host)

	if err := e.Send(m.Addr, smtpAuth); err != nil {
		log.Debug().Err(err).Msg("Failed to send password reset email")
		return err
	}

	log.Debug().Msg("Successfully sent password reset email")

	return nil
}
