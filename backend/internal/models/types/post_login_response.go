package types

import "bitbucket.org/usamahamid007/us_ha/internal/models"

type PostLoginResponse struct {
	JWTToken string      `json:"jwtToken"`
	User     models.User `json:"user"`
}
