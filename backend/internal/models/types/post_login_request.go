package types

type PostLoginRequest struct {
	Email      string     `json:"email"`
	Password   string     `json:"password"`
}