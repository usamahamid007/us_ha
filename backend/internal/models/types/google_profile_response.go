package types

type GoogleProfileResponse struct {
	Sub             string `json:"sub"`
	FullName        string `json:"name"`
	GivenName       string `json:"given_name"`
	FamilyName      string `json:"family_name"`
	Profile         string `json:"profile"`
	PictureURL      string `json:"picture"`
	Email           string `json:"email"`
	IsEmailVerified bool   `json:"email_verified"`
	Gender          bool   `json:"gender"`
}
