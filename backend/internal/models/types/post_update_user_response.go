package types

import "bitbucket.org/usamahamid007/us_ha/internal/models"

type PostUpdateUserResponse struct {
	User models.User `json:"user"`
}
