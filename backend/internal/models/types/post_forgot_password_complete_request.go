package types

type PostForgotPasswordCompleteRequest struct {
	Token    string `json:"token"`
	Password string `json:"password"`
}
