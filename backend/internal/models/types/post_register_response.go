package types

import "bitbucket.org/usamahamid007/us_ha/internal/models"

type PostRegisterResponse struct {
	JWTToken string      `json:"jwtToken"`
	User     models.User `json:"user"`
}
