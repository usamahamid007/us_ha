package types

type PostUpdateUserRequest struct {
	Email      string     `json:"email"`
	FullName   string     `json:"full_name"`
	Address    string     `json:"address"`
	Phone      string     `json:"phone"`
}