package types

type PostRegisterRequest struct {
	Email      string     `json:"email"`
	Password   string     `json:"password"`
	FullName   string     `json:"full_name"`
	Address    string     `json:"address"`
	Phone      string     `json:"phone"`
}