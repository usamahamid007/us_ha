package types

type PostForgotPasswordRequest struct {
	Email      string     `json:"email"`
}