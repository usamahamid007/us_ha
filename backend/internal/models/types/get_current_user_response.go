package types

import "bitbucket.org/usamahamid007/us_ha/internal/models"

type GetCurrentUserResponse struct {
	User models.User `json:"user"`
}
