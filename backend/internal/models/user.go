package models

import (
	"gorm.io/gorm"
)

type User struct {
	gorm.Model
	Email    string `gorm:"unique_index" json:"email"`
	Password string `gorm:"type:varbinary(64)" json:"-"`
	Provider string `gorm:"type:varchar(64)" json:"provider"`
	FullName string `gorm:"type:varchar(256)" json:"full_name"`
	Address  string `json:"address"`
	Phone    string `gorm:"type:varchar(24)" json:"phone"`
}
