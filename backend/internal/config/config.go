package config

import (
	"bitbucket.org/usamahamid007/us_ha/internal/util"
	"github.com/joho/godotenv"
	"github.com/rs/zerolog"
	"os"
	"sync"
	"time"
)

var (
	config     ServerConfig
	configOnce sync.Once
)

type DatabaseConfig struct {
	DSN         string
	AutoMigrate bool
}

type GoogleAuthConfig struct {
	ClientID     string
	ClientSecret string
	RedirectURL  string
}

type LoggerConfig struct {
	Level              zerolog.Level
	PrettyPrintConsole bool
}

type MailerConfig struct {
	Host            string
	Port            string
	Username        string
	Password        string `json:"-"`
	UseTLS          bool
	BaseTemplateDir string
	DefaultSender   string
}

type ServerConfig struct {
	Port                  string
	JWTSecret             []byte
	JWTExpiry             time.Duration
	ResetPasswordURL      string
	AuthRedirectURL       string
	ResetPasswordDuration time.Duration

	DBConfig      DatabaseConfig
	GglAuthConfig GoogleAuthConfig
	Logger        LoggerConfig
	Mailer        MailerConfig
}

func DefaultServiceConfigFromEnv() ServerConfig {
	configOnce.Do(func() {
		config = ServerConfig{
			Port:                  util.GetEnv("PORT", "8080"),
			JWTSecret:             []byte(os.Getenv("JWT_SECRET")),
			JWTExpiry:             time.Minute * time.Duration(util.GetEnvAsInt("JWT_EXPIRY_IN_MINS", 60)),
			ResetPasswordURL:      os.Getenv("RESET_PASSWORD_URL"),
			AuthRedirectURL:       os.Getenv("AUTH_REDIRECT_URL"),
			ResetPasswordDuration: time.Second * time.Duration(util.GetEnvAsInt("RESET_PASSWORD_VALIDITY", 900)),
			DBConfig: DatabaseConfig{
				DSN:         os.Getenv("DSN"),
				AutoMigrate: util.GetEnvAsBool("DB_AUTO_MIGRATE", false),
			},
			GglAuthConfig: GoogleAuthConfig{
				ClientID:     os.Getenv("GGL_AUTH_CLIENT_ID"),
				ClientSecret: os.Getenv("GGL_AUTH_CLIENT_SECRET"),
				RedirectURL:  util.GetEnv("GGL_AUTH_REDIRECT_URL", zerolog.DebugLevel.String()),
			},
			Logger: LoggerConfig{
				Level:              util.LogLevelFromString(util.GetEnv("LOGGER_LEVEL", zerolog.DebugLevel.String())),
				PrettyPrintConsole: util.GetEnvAsBool("LOGGER_PRETTY_PRINT_CONSOLE", false),
			},
			Mailer: MailerConfig{
				Host:            os.Getenv("SMTP_HOST"),
				Port:            util.GetEnv("SMTP_PORT", "587"),
				Username:        os.Getenv("SMTP_USER_EMAIL"),
				Password:        os.Getenv("SMTP_USER_PASSWORD"),
				UseTLS:          util.GetEnvAsBool("SMTP_USE_TLS", false),
				BaseTemplateDir: os.Getenv("EMAIL_TEMPLATES_ABS_DIR"),
				DefaultSender:   util.GetEnv("SMTP_DEFAULT_FROM_EMAIL", "Usama Hamid <usamahamid007@gmail.com>"),
			},
		}
	})
	return config
}

func LoadEnvFile() error {
	err := godotenv.Load()
	return err
}
