package util

import (
	"github.com/dgrijalva/jwt-go"
	"strconv"
	"time"
)

const (
	UserIdKey = "userId"
	EmailKey  = "email"
)


func GenerateToken(userId uint, email string, jwtSecret []byte, expiry time.Duration) (string, error) {
	expirationTime := time.Now().Add(expiry)
	// Create the JWT claims, which includes the username and expiry time
	claims := &jwt.MapClaims{
		UserIdKey: strconv.Itoa(int(userId)),
		EmailKey:  email,
		"exp":     expirationTime.Unix(),
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString(jwtSecret)
	return tokenString, err
}

func ValidateToken(tokenString string, jwtSecret []byte, expiry time.Duration) (jwt.Claims, error) {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		return jwtSecret, nil
	})
	if err != nil {
		return nil, err
	}
	return token.Claims, err
}
