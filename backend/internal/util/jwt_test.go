package util_test

import (
	"bitbucket.org/usamahamid007/us_ha/internal/util"
	"github.com/dgrijalva/jwt-go"
	"github.com/stretchr/testify/assert"
	"strconv"
	"testing"
	"time"
)

func TestClaimsRetrieval(t *testing.T) {
	t.Parallel()

	randomId := uint(86453)
	randomEmail := "fd2bvihplxyx9zgw3plw@uywpa.net"
	secret1 := []byte("F1Vj9hnuAou0dfvzJTaW")
	duration := time.Second * time.Duration(900)
	token1, _ := util.GenerateToken(randomId, randomEmail, secret1, duration)
	claims, _ := util.ValidateToken(token1, secret1, duration)

	userId := claims.(jwt.MapClaims)[util.UserIdKey].(string)
	email := claims.(jwt.MapClaims)[util.EmailKey].(string)
	u64, err := strconv.ParseUint(userId, 10, 32)
	assert.Nil(t, err)

	assert.Equal(t, randomId, uint(u64))
	assert.Equal(t, randomEmail, email)
}

func TestTokenExpiry(t *testing.T) {
	t.Parallel()

	randomId := uint(86453)
	randomEmail := "fd2bvihplxyx9zgw3plw@uywpa.net"
	secret1 := []byte("F1Vj9hnuAou0dfvzJTaW")
	durationInt := 2
	duration := time.Second * time.Duration(durationInt)
	token1, _ := util.GenerateToken(randomId, randomEmail, secret1, duration)

	time.Sleep(time.Second * time.Duration(durationInt+1))

	_, err := util.ValidateToken(token1, secret1, duration)
	assert.NotNil(t, err)
	assert.Equal(t, "Token is expired", err.Error())
}
