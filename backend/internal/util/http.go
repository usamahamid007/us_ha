package util

import (
	"bitbucket.org/usamahamid007/us_ha/internal/models/types"
	"encoding/json"
	"github.com/rs/zerolog/log"
	"net/http"
)

func ParseJSONRequest(r *http.Request, payload interface{}) {
	err := json.NewDecoder(r.Body).Decode(&payload)
	if err != nil {
		log.Warn().Err(err).Msg("Error during parsing request payload")
	}
}

func MarshalJSONResponse(w http.ResponseWriter, response interface{}) {
	js, err := json.Marshal(response)
	if err != nil {
		log.Error().Err(err).Msg("Error during parsing error msg response")
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}

func HandleErrorResponse(w http.ResponseWriter, statusCode int, error string) {
	log.Debug().Msg("Responding with error: " + error)
	w.WriteHeader(statusCode)
	response := types.CommonMsgResponse{Msg: error}
	js, err := json.Marshal(response)
	if err != nil {
		log.Error().Err(err).Msg("Error during parsing error msg response")
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}
