package util

import (
	"golang.org/x/crypto/bcrypt"
)

func HashPassword(password string) (hash string, err error) {
	byteHash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.MinCost)
	return string(byteHash), err
}

func ComparePasswordAndHash(password string, hash string) (err error) {
	err = bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err
}