package util_test

import (
	"bitbucket.org/usamahamid007/us_ha/internal/util"
	"github.com/stretchr/testify/assert"
	"os"
	"testing"
)

func TestGetEnv(t *testing.T) {
	t.Parallel()

	envVar := "TESTING_ENVIRONMENT_VARIABLE_STR"
	randomStr1 := "uLfsdpuwOI"
	res := util.GetEnv(envVar, randomStr1)
	assert.Equal(t, randomStr1, res)

	randomStr2 := "evFoqsehgj"
	os.Setenv(envVar, randomStr2)
	defer os.Unsetenv(envVar)
	res = util.GetEnv(envVar, randomStr1)
	assert.Equal(t, randomStr2, res)
}

func TestGetEnvAsBool(t *testing.T) {
	t.Parallel()

	envVar := "TESTING_ENVIRONMENT_VARIABLE_BOOL"
	res := util.GetEnvAsBool(envVar, true)
	assert.Equal(t, true, res)

	os.Setenv(envVar, "true")
	res = util.GetEnvAsBool(envVar, false)
	assert.Equal(t, true, res)

	os.Setenv(envVar, "f")
	defer os.Unsetenv(envVar)
	res = util.GetEnvAsBool(envVar, true)
	assert.Equal(t, false, res)

	os.Setenv(envVar, "1")
	res = util.GetEnvAsBool(envVar, false)
	assert.Equal(t, true, res)
}