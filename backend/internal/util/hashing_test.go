package util_test

import (
	"bitbucket.org/usamahamid007/us_ha/internal/util"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestHash(t *testing.T) {
	t.Parallel()

	randomStr1 := "nvhhEbb1h4"
	hash1, _ := util.HashPassword(randomStr1)
	hash2, _ := util.HashPassword(randomStr1)

	assert.NotEqual(t, hash1, hash2)

	err1 := util.ComparePasswordAndHash(randomStr1, hash1)
	assert.Nil(t, err1)

	err2 := util.ComparePasswordAndHash(randomStr1, hash2)
	assert.Nil(t, err2)

	err3 := util.ComparePasswordAndHash("nvhhEbb1h", hash1)
	assert.Error(t, err3)
}
