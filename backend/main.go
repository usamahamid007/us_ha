package main

import (
	"bitbucket.org/usamahamid007/us_ha/internal/api"
	"bitbucket.org/usamahamid007/us_ha/internal/api/router"
	"bitbucket.org/usamahamid007/us_ha/internal/config"
	"context"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func main() {
	zerolog.TimeFieldFormat = time.RFC3339Nano

	if err := config.LoadEnvFile(); err != nil {
		log.Fatal().Msg("Error loading .env file")
	}

	serverConfig := config.DefaultServiceConfigFromEnv()
	zerolog.SetGlobalLevel(serverConfig.Logger.Level)
	if serverConfig.Logger.PrettyPrintConsole {
		log.Logger = log.Output(zerolog.NewConsoleWriter(func(w *zerolog.ConsoleWriter) {
			w.TimeFormat = "15:04:05"
		}))
	}

	s := api.NewServer(serverConfig)

	// Initialize DB with timeout
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	if err := s.InitDB(ctx); err != nil {
		cancel()
		log.Fatal().Err(err).Msg("Failed to initialize database")
	}
	cancel()

	s.InitOAuthConfig()

	if err := s.InitMailer(); err != nil {
		log.Fatal().Err(err).Msg("Failed to initialize mailer")
	}

	router.Init(s)

	addr := ":" + serverConfig.Port
	httpServer := &http.Server{Addr: addr, Handler: s.MuxRouter}

	done := make(chan os.Signal, 1)
	signal.Notify(done, os.Interrupt, syscall.SIGTERM)
	go func() {
		if err := httpServer.ListenAndServe(); err != nil {
			if err == http.ErrServerClosed {
				log.Info().Msg("Server close triggered!")
			} else {
				log.Fatal().Err(err).Msg("Failed to start server")
			}
		}
	}()
	<-done

	ctx, cancel = context.WithTimeout(context.Background(), 5*time.Second)
	defer func() {
		s.HandleShutdown()
		cancel()
	}()

	if err := httpServer.Shutdown(ctx); err != nil && err != http.ErrServerClosed {
		log.Fatal().Err(err).Msg("Failed to gracefully shut down server")
	}
	log.Info().Msg("Server exited gracefully")
}
